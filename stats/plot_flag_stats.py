from pathlib import Path

import pandas as pd
from matplotlib import pyplot as plt

TMP_PATH = Path("tmp")
INPUT_PATH = TMP_PATH / "flagged_notices.csv"
OUTPUT_PATH = TMP_PATH / "activated_flags.png"


def main():
    df = pd.read_csv(INPUT_PATH)
    data = {
        column: df[column].sum()
        for column in list(df.columns)[2:9]
    }
    rects = plt.bar(data.keys(), data.values())
    plt.bar_label(rects)
    plt.xticks(rotation=45, ha="right", rotation_mode="anchor")
    plt.title("Number of notices for each flag")
    plt.xlabel("Flag")
    plt.ylabel("Number of notices")
    plt.savefig(OUTPUT_PATH, bbox_inches="tight")


if __name__ == '__main__':
    main()
