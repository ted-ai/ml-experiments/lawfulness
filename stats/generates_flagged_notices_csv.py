import csv
import re
from decimal import Decimal
from pathlib import Path
from typing import Iterator

from pydantic import BaseModel

CONTEXT_EXTENSION_LENGTH = 20
FORBIDDEN_COUNTRY_CODES = ["ru", "by"]
LEGAL_TYPES = ["body-pl", "body-pl-cga", "body-pl-la", "body-pl-ra", "cga", "det-cont", "eu-ins-bod-ag", "grp-p-aut",
               "int-org", "la", "org-sub", "org-sub-cga", "org-sub-la", "org-sub-ra", "pub-undert", "pub-undert-cga",
               "pub-undert-la", "pub-undert-ra", "ra", "spec-rights-entity"]

TMP_PATH = Path("tmp")
INPUT_PATH = TMP_PATH / "flagged_notices.jsonl"
OUTPUT_PATH = TMP_PATH / "flagged_notices.csv"

TEST_WORD_PATTERN = re.compile(r"\btest\b", flags=re.IGNORECASE)
SALE_WORD_PATTERN = re.compile(r"\bsale\b", flags=re.IGNORECASE)


class ContractingBody(BaseModel):
    name: str | None
    town: str | None
    postal_code: str | None
    country: str | None


class NoticeFields(BaseModel):
    countries: list[str]
    languages: list[str]
    buyer_types: list[str]
    contracting_bodies: list[ContractingBody]
    eu_funds: list[str]
    free_texts: list[str]
    processed_free_texts: list[str]
    translated_free_texts: list[str]
    processed_translated_free_texts: list[str]


class NoticeFieldAnalysis(BaseModel):
    has_test_word: bool
    has_sale_word: bool
    incoherent_texts: list[str]
    is_forbidden_country: bool
    non_eu_countries: list[str]
    is_not_funded_by_eu: bool
    is_contracting_body_private: bool
    is_contracting_body_international: bool
    contracting_body_whitelist_score: Decimal
    is_contracting_body_whitelisted: bool


class NoticeFlags(BaseModel):
    has_test_word: bool
    has_sale_word: bool
    has_incoherent_text: bool
    is_forbidden_country: bool
    has_non_eu_country_and_not_funded_by_eu: bool
    is_contracting_body_private_and_not_funded_by_eu: bool
    has_not_whitelisted_international_contracting_body: bool


class NoticeResult(BaseModel):
    notice_id: str
    notice_publication_number: str | None
    flags: NoticeFlags
    field_analysis: NoticeFieldAnalysis
    extracted_fields: NoticeFields


def main():
    with OUTPUT_PATH.open('w') as output_file:
        writer = csv.writer(output_file)
        writer.writerow((
            "Notice ID",
            "Notice number",
            "'Test' word",
            "'Sale' word",
            "Incoherent text",
            "Forbidden country",
            "Non EU country + not funded by EU",
            "Private contracting body + not funded by EU",
            "International and not whitelisted contracting body",
            "Reasons"
        ))
        for line in INPUT_PATH.read_text().splitlines():
            notice = NoticeResult.model_validate_json(line)
            writer.writerow((
                notice.notice_id,
                notice.notice_publication_number,
                notice.flags.has_test_word,
                notice.flags.has_sale_word,
                notice.flags.has_incoherent_text,
                notice.flags.is_forbidden_country,
                notice.flags.has_non_eu_country_and_not_funded_by_eu,
                notice.flags.is_contracting_body_private_and_not_funded_by_eu,
                notice.flags.has_not_whitelisted_international_contracting_body,
                generate_reasons(notice)
            ))


def generate_reasons(notice: NoticeResult) -> str:
    reasons = []
    if notice.flags.has_test_word:
        reasons.extend(test_word_reason(notice))
    if notice.flags.has_sale_word:
        reasons.extend(sale_word_reason(notice))
    if notice.flags.has_incoherent_text:
        reasons.extend(incoherent_text_reason(notice))
    if notice.flags.is_forbidden_country:
        reasons.extend(forbidden_country_reason(notice))
    if notice.flags.has_non_eu_country_and_not_funded_by_eu:
        reasons.extend(non_eu_country_and_not_funded_by_eu_reason(notice))
    if notice.flags.is_contracting_body_private_and_not_funded_by_eu:
        reasons.extend(contracting_body_private_and_not_funded_by_eu_reason(notice))
    if notice.flags.has_not_whitelisted_international_contracting_body:
        reasons.extend(not_whitelisted_international_contracting_body_reason(notice))
    return "\n".join(reasons)


def test_word_reason(notice: NoticeResult) -> Iterator[str]:
    for free_texts in zip(
            notice.extracted_fields.free_texts,
            notice.extracted_fields.processed_free_texts,
            notice.extracted_fields.translated_free_texts,
            notice.extracted_fields.processed_translated_free_texts,
    ):
        for free_text in free_texts:
            if TEST_WORD_PATTERN.search(free_text):
                yield f"'test' word found in free text '{limit_context(free_text, 'test')}'"
                break


def sale_word_reason(notice: NoticeResult) -> Iterator[str]:
    for free_texts in zip(
            notice.extracted_fields.free_texts,
            notice.extracted_fields.processed_free_texts,
            notice.extracted_fields.translated_free_texts,
            notice.extracted_fields.processed_translated_free_texts,
    ):
        for free_text in free_texts:
            if SALE_WORD_PATTERN.search(free_text):
                yield f"'sale' word found in free text '{limit_context(free_text, 'sale')}'"
                break


def incoherent_text_reason(notice: NoticeResult) -> Iterator[str]:
    for incoherent_text in set(notice.field_analysis.incoherent_texts):
        for free_texts in zip(
                notice.extracted_fields.free_texts,
                notice.extracted_fields.processed_free_texts,
                notice.extracted_fields.translated_free_texts,
                notice.extracted_fields.processed_translated_free_texts,
        ):
            for free_text in free_texts:
                if incoherent_text.lower() in free_text.lower():
                    context = limit_context(free_text, incoherent_text)
                    yield f"Incoherent text '{incoherent_text}' found in free texts '{context}'"
                    break


def forbidden_country_reason(notice: NoticeResult) -> Iterator[str]:
    for country in FORBIDDEN_COUNTRY_CODES:
        if country in notice.extracted_fields.countries:
            yield f"Forbidden country '{country}' found"


def non_eu_country_and_not_funded_by_eu_reason(notice: NoticeResult) -> Iterator[str]:
    if "no-eu-funds" in notice.extracted_fields.eu_funds:
        yield f"Non EU countries {notice.field_analysis.non_eu_countries} found and EU fund 'no-eu-funds' specified"
    else:
        yield f"Non EU countries {notice.field_analysis.non_eu_countries} found and no EU fund specified"


def contracting_body_private_and_not_funded_by_eu_reason(notice: NoticeResult) -> Iterator[str]:
    invalid_buyer_types = [t for t in notice.extracted_fields.buyer_types if t not in LEGAL_TYPES]
    if "no-eu-funds" in notice.extracted_fields.eu_funds:
        yield f"Unknown buyer types {invalid_buyer_types} found and EU fund 'no-eu-funds' specified"
    else:
        yield f"Unknown buyer types {invalid_buyer_types} found and no EU fund specified"


def not_whitelisted_international_contracting_body_reason(notice: NoticeResult) -> Iterator[str]:
    yield ("'int-org' buyer type found and whitelist score of "
           f"{notice.field_analysis.contracting_body_whitelist_score:.3f} below threshold")


def limit_context(text: str, subpart: str) -> str:
    start_pos = text.lower().index(subpart.lower())
    end_pos = start_pos + len(subpart)
    extended_start_pos = max(0, start_pos - CONTEXT_EXTENSION_LENGTH)
    extended_end_pos = end_pos + CONTEXT_EXTENSION_LENGTH
    prefix = "..." if extended_start_pos > 0 else ""
    suffix = "..." if extended_end_pos < len(text) else ""
    return prefix + text[extended_start_pos:extended_end_pos] + suffix


if __name__ == '__main__':
    main()
