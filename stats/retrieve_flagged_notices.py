import json
from pathlib import Path
from typing import Any

import boto3
from boto3.dynamodb.types import TypeDeserializer

TABLE_NAME = "d-ew1-ted-ai-lawfulness-flagged-notices"
TMP_PATH = Path("tmp")
OUTPUT_PATH = TMP_PATH / "flagged_notices.jsonl"

DYNAMODB = boto3.client('dynamodb')


def main():
    items = retrieve_flagged_notices()
    items = clean_dynamodb_items(items)
    TMP_PATH.mkdir(exist_ok=True)
    save_flagged_notices(items)


def retrieve_flagged_notices():
    items = []
    last_evaluated_key = None
    while True:
        if last_evaluated_key:
            response = DYNAMODB.scan(TableName=TABLE_NAME, ExclusiveStartKey=last_evaluated_key)
        else:
            response = DYNAMODB.scan(TableName=TABLE_NAME)
        items.extend(response['Items'])
        last_evaluated_key = response.get('LastEvaluatedKey')
        if not last_evaluated_key:
            break
    return items


def clean_dynamodb_items(items: list[dict[str, Any]]) -> list[dict[str, Any]]:
    deserializer = TypeDeserializer()
    return [
        {key: deserializer.deserialize(value) for key, value in item.items()}
        for item in items
    ]


def save_flagged_notices(items: list[dict[str, Any]]):
    with OUTPUT_PATH.open('w') as f:
        for item in items:
            f.write(json.dumps(item) + '\n')


if __name__ == '__main__':
    main()
