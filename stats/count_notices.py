from datetime import datetime

import boto3

LOG_GROUP = "/tedai/lawfulness"
LOG_STREAMS = [
    "ecs/lawfulness/1d48cc5be37243d1b0ea8144d94516e2",
    "ecs/lawfulness/209b1f663d8547e3b6bd9cdcfd33c0e3",
    "ecs/lawfulness/51d0277ae8564ecc879a8fc1353217da",
    "ecs/lawfulness/60063b7515054c29bd41a597db12d1d9",
    "ecs/lawfulness/71597852ec3e4df89878cd8799d51f19",
    "ecs/lawfulness/ec15d0575ef94a3a8b6a4e65f456a83a",
    "ecs/lawfulness/f66f923d7770489ab147bbc7ddd8c219",
    "ecs/lawfulness/ff2858188c3f4441bba7357d1ec58af9",
]
LOGS = boto3.client("logs")


def main():
    total_count = 0
    eforms_count = 0
    treated_count = 0
    min_time = 2 ** 64
    max_time = 0
    for log_stream in LOG_STREAMS:
        next_token = None
        while True:
            params = {
                'logGroupName': LOG_GROUP,
                'logStreamName': log_stream,
                'startFromHead': True,
            }
            if next_token:
                params['nextToken'] = next_token
            response = LOGS.get_log_events(**params)
            print(f"Retrieved {len(response['events'])} events from {log_stream}...")
            if len(response['events']) == 0:
                break
            for event in response.get('events', []):
                min_time = min(min_time, event['timestamp'])
                max_time = max(max_time, event['timestamp'])
                message = event['message']
                if 'Notices downloaded from s3' in message:
                    total_count += 1
                if 'detected as eForms' in message:
                    eforms_count += 1
                if 'Computed flags for notice' in message:
                    treated_count += 1
            next_token = response.get('nextForwardToken')
    print("Total number of notices:", total_count)
    print("Total number of eForms notices:", eforms_count)
    print("Total number of treated notices:", treated_count)
    print("Start time:", datetime.fromtimestamp(min_time / 1000).isoformat())
    print("End time:", datetime.fromtimestamp(max_time / 1000).isoformat())
    print("Time:", (max_time - min_time) / 1000 / 60 / 60, "hours")


if __name__ == '__main__':
    main()
